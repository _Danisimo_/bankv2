import java.lang.reflect.AccessibleObject;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Acount implements Runnable {
    private int balanc = 0;
    private String name;
    private static int counter = 0;

    private boolean ready = false;

    private boolean creditvzav = false;

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Acount.counter = counter;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public boolean isCreditvzav() {
        return creditvzav;
    }

    public Acount() {
    }

    public void setCreditvzav(boolean creditvzav) {
        this.creditvzav = creditvzav;
    }

    public boolean isSplatuv() {
        return splatuv;
    }

    public void setSplatuv(boolean splatuv) {
        this.splatuv = splatuv;
    }

    public boolean isDepvz() {
        return depvz;
    }

    public void setDepvz(boolean depvz) {
        this.depvz = depvz;
    }

    public boolean isDepvid() {
        return depvid;
    }

    public void setDepvid(boolean depvid) {
        this.depvid = depvid;
    }

    private boolean splatuv = false;

    private boolean depvz = false;
    private boolean depvid = false;

    public Acount(String name) {
        this.name = name;
    }

    public int getBalanc() {
        return balanc;
    }

    public void setBalanc(int balanc) {
        this.balanc = balanc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public void run() {
        System.out.println("-");
        if (counter < 6) {
            System.out.println("+");

            Random random = new Random();

            this.setName("name-" + random.nextInt());
            int rand = random.nextInt();

            while (!Bank.isIsAvailable1() && !Bank.isIsAvailable2()){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (rand > 0.5 && Bank.isIsAvailable1()) {
                Bank.credit(this, random.nextInt(1500) + 500);
                try {
                    Thread.sleep(random.nextInt(2) + 1);
                    this.ready = true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                this.creditvzav = true;
                FileUtil.toCsv(this);
            } else if (rand < 0.5 && Bank.isIsAvailable2()) {
                Bank.deposite(this, random.nextInt(1000) + 1000);
                try {
                    Thread.sleep(random.nextInt(2) + 1);
                    this.ready = true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                this.depvz = true;
                FileUtil.toCsv(this);

            }

            AcountHolder.addAcountToList(this);
            counter++;
        }
        else{
            System.out.println("=");
            Thread.currentThread().stop();
        }

    }
}
