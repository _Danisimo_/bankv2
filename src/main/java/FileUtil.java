import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtil {

    public static void toCsv(Acount acount){
        File file = new File("cv.csv");

        StringBuffer sb = null;
        if (file.length()!=0) {
            sb = new StringBuffer("");
        } else {
            sb = new StringBuffer("name, type, sum \n");
        }



            sb.append((acount.getName())).append(", ").append(acount.getBalanc()<0?"Deposit":"Credit").append(", ").append((acount.getBalanc())).append("\n");

        try (FileWriter writer = new FileWriter(file, true)) {
            writer.write(String.valueOf(sb));
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }


}
