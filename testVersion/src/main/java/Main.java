import java.io.File;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;




public class Main  {

    public static int counter = 0;

    public static synchronized int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Main.counter = counter;
    }

    public static void main(String[] args) throws InterruptedException  {


        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
        ScheduledExecutorService executorService1 = Executors.newScheduledThreadPool(1);
        ScheduledExecutorService executorService2 = Executors.newScheduledThreadPool(1);


        CountDownLatch countDownLatch = new CountDownLatch(5);
        CountDownLatch countDownLatch1 = new CountDownLatch(1);
        executorService.scheduleAtFixedRate(()->new Thread(new Acount("", countDownLatch)).start(), 0, 10, TimeUnit.MILLISECONDS);

        executorService1.scheduleAtFixedRate(new Checker(), 0, 10, TimeUnit.MILLISECONDS);

        executorService2.scheduleAtFixedRate(new Casher(countDownLatch1), 0, 30, TimeUnit.MILLISECONDS);

        countDownLatch.await();
        executorService.shutdownNow();
        countDownLatch1.await();
        executorService1.shutdownNow();



    }

    public static synchronized void incrementCount() {
        counter++;
    }
}
