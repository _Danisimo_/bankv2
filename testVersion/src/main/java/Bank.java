import java.sql.Time;
import java.util.Random;

public class Bank {
    private static volatile int balnce = 500000000;

    private static volatile int creditCount = 0;
    private static volatile int creditAmount = 0;

    private static volatile int depositeCount = 0;
    private static volatile int depositeAmount = 0;

    private static volatile boolean isAvailable1 = true;
    private static volatile boolean isAvailable2 = true;

    public static synchronized void credit(Acount acount, Integer sum){

        acount.setBalanc(acount.getBalanc()+sum);
        balnce = balnce - sum;
        creditCount++;
        creditAmount = creditCount + sum;
        System.out.println("сума депозита " +sum);
        System.out.println("баланс банка "+balnce);
        new Thread(()->FileUtil.toCsv(acount)).start();
        System.out.println("Credit : " + acount.getName());

    }

    public static synchronized void creditTo(Acount acount){

        balnce = balnce + acount.getBalanc() + acount.getBalanc()*5/100;
        creditCount--;
        creditAmount = creditAmount + acount.getBalanc() + acount.getBalanc()*5/100;
        System.out.println(acount.getBalanc() + acount.getBalanc()*5/100);
        System.out.println("Credit otdav : " + acount.getName());

    }

    public static synchronized void deposite(Acount acount, Integer sum){
        acount.setBalanc(acount.getBalanc()-sum);
        depositeCount++;
        depositeAmount = depositeAmount + sum;
        balnce = balnce + sum;
        System.out.println("сума депозита " +sum);
        System.out.println("баланс банка "+balnce);
        new Thread(()->FileUtil.toCsv(acount)).start();
        System.out.println("Deposite : "  + acount.getName());
    }

    public static synchronized void depositeTo(Acount acount){
        depositeCount--;
        depositeAmount = depositeAmount - acount.getBalanc() -acount.getBalanc()*2/100;
        balnce = balnce - acount.getBalanc() -acount.getBalanc()*2/100;
        System.out.println(acount.getBalanc()+acount.getBalanc()*2/100);
        System.out.println("Deposite zabrav : "  + acount.getName());
    }

    public static synchronized boolean creditIsAvaible(){

        if (balnce<2*(depositeAmount/depositeCount)){
            return false;
        }
        return true;
    }

    public static synchronized boolean depositeIsAvaible(){
        if (balnce>=1000000){
            return false;
        }
        return true;
    }

    public static int getBalnce() {
        return balnce;
    }

    public static void setBalnce(int balnce) {
        Bank.balnce = balnce;
    }

    public static int getCreditCount() {
        return creditCount;
    }

    public static int getCreditAmount() {
        return creditAmount;
    }

    public static int getDepositeCount() {
        return depositeCount;
    }

    public static int getDepositeAmount() {
        return depositeAmount;
    }

    public static void setCreditCount(int creditCount) {
        Bank.creditCount = creditCount;
    }

    public static void setCreditAmount(int creditAmount) {
        Bank.creditAmount = creditAmount;
    }

    public static void setDepositeCount(int depositeCount) {
        Bank.depositeCount = depositeCount;
    }

    public static void setDepositeAmount(int depositeAmount) {
        Bank.depositeAmount = depositeAmount;
    }

    public static boolean isIsAvailable1() {
        return isAvailable1;
    }

    public static void setIsAvailable1(boolean isAvailable1) {
        Bank.isAvailable1 = isAvailable1;
    }

    public static boolean isIsAvailable2() {
        return isAvailable2;
    }

    public static void setIsAvailable2(boolean isAvailable2) {
        Bank.isAvailable2 = isAvailable2;
    }
}
